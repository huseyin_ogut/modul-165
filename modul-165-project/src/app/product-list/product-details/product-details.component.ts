import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { Product } from 'src/app/models/product.model';
import { ShoppingCartService } from 'src/app/shopping-cart/shopping-cart.service';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  productId: string;
  product: Product;
  placeHolderProduct: Product = {
    name: '',
    description: '',
    price: null,
    imageUrl: '',
    category: ''
  };
  editedProduct: Product;
  isEditing: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService,
    private shoppingCartService: ShoppingCartService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.product = this.placeHolderProduct;
    this.productId = this.route.snapshot.paramMap.get('id');
    this.getProductById();
  }

  getProductById(): void {
    this.productsService
      .getById(this.productId)
      .then((product: Product | undefined) => {
        if (product) {
          this.product = product;
          this.editedProduct = { ...product }; // Copy product for editing
        } else {
          this.product = null;
        }
      })
      .catch((error: any) => {
        console.error('Error getting product:', error);
      });
  }

  editProduct(): void {
    this.isEditing = true;
  }

  updateProduct(): void {
    this.productsService
      .updateProduct(this.editedProduct)
      .then(() => {
        this.product = { ...this.editedProduct }; // Update original product with edited values
        this.isEditing = false;
      })
      .catch((error: any) => {
        console.error('Error updating product:', error);
      });
  }

  cancelEdit(): void {
    this.isEditing = false;
    this.editedProduct = { ...this.product }; // Reset edited product
  }

  deleteProduct(): void {
    this.productsService
      .deleteProduct(this.productId)
      .then(() => {
        console.log('Product deleted successfully');
        this.router.navigate(['/products']);
      })
      .catch((error: any) => {
        console.error('Error deleting product:', error);
      });
  }

  addToCart(): void {
    const cartId = 'ogmkYzx5YMzDDlz37ywp'; // Replace with your actual cartId
    const productIds = [this.productId];

    console.log(productIds);

    this.shoppingCartService
      .addProductIdsToCart(cartId, productIds)
      .then(() => {
        console.log('Product added to cart successfully');
        this.router.navigate(['/cart']);
      })
      .catch((error: any) => {
        console.error('Error adding product to cart:', error);
      });


  }
}