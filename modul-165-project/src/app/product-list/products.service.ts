import { Injectable, inject } from '@angular/core';
import { Firestore, QuerySnapshot, addDoc, collection, collectionData, deleteDoc, doc, getDoc, getDocs, setDoc } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Product } from '../models/product.model';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products$: Observable<any[]>;
  firestore: Firestore = inject(Firestore);

  constructor() { }

  async getProducts(): Promise<Product[]> {
    const querySnapshot: QuerySnapshot = await getDocs(collection(this.firestore, 'products'));
    const products: Product[] = [];
  
    querySnapshot.forEach((doc) => {
      const productData = doc.data() as Product;
      const product: Product = {
        id: doc.id,
        ...productData
      };
      products.push(product);
    });
  
    return products;
  }

  async createProduct(product: Product): Promise<void> {
    try {
      const docRef = await addDoc(collection(this.firestore, "products"), product);
      console.log("Product created with ID: ", docRef.id);
    } catch (error) {
      console.error("Error creating product: ", error);
    }
  }

  async getById(productId: string): Promise<Product | undefined> {
    try {
      const docRef = doc(this.firestore, "products", productId);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        const productData = docSnap.data();
        const product: Product = {
          id: productId,
          name: productData.name,
          description: productData.description,
          price: productData.price,
          category: productData.category,
          imageUrl: productData.imageUrl
        };
        return product;
      } else {
        console.log("Product does not exist");
        return undefined;
      }
    } catch (error) {
      console.error("Error getting product: ", error);
      return undefined;
    }
  }

  async updateProduct(product: Product): Promise<void> {
    try {
      const docRef = doc(this.firestore, "products", product.id);
      await setDoc(docRef, product);
      console.log("Product updated successfully");
    } catch (error) {
      console.error("Error updating product: ", error);
    }
  }

  async deleteProduct(productId: string): Promise<void> {
    try {
      const docRef = doc(this.firestore, "products", productId);
      await deleteDoc(docRef);
      console.log("Product deleted successfully");
    } catch (error) {
      console.error("Error deleting product: ", error);
    }
  }
}
