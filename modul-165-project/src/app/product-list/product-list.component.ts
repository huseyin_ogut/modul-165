import { Component } from '@angular/core';
import { ProductsService } from './products.service';
import { Product } from '../models/product.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent {

  public products: Product[] = [];
  public newProduct: Product = {
    name: '',
    description: '',
    price: 0,
    category: '',
    imageUrl: ''
  };
  public showForm = false;
  private defaultImageUrl = 'https://media.istockphoto.com/id/1147544807/de/vektor/miniaturbild-vektorgrafik.jpg?s=612x612&w=0&k=20&c=IIK_u_RTeRFyL6kB1EMzBufT4H7MYT3g04sz903fXAk=';


  constructor(private productsService: ProductsService) {

  }

  async ngOnInit(): Promise<void> {
    await this.getProducts();
    // Wird nur zum erstellen der initalen Produkte verwendet.
    // this.createProduct();
  }

  async getProducts(): Promise<void> {
    try {
      const productsData: any[] = await this.productsService.getProducts();
      this.products = productsData.map((productData: any) => {
        const product: Product = {
          id: productData.id,
          name: productData.name,
          description: productData.description,
          price: productData.price,
          category: productData.category,
          imageUrl: productData.imageUrl
        };
        return product;
      });
    } catch (error) {
      console.error('Error getting products:', error);
    }
  }

  async createProduct(): Promise<void> {
    const exampleImageUrl = 'https://media.istockphoto.com/id/1147544807/de/vektor/miniaturbild-vektorgrafik.jpg?s=612x612&w=0&k=20&c=IIK_u_RTeRFyL6kB1EMzBufT4H7MYT3g04sz903fXAk=';

    const productData = [
      {
        "name": "Smartwatch X1",
        "description": "Die Smartwatch X1 ist eine hochwertige Smartwatch mit einem stilvollen Design und zahlreichen nützlichen Funktionen. Mit einem integrierten Schrittzähler kannst du deine täglichen Aktivitäten verfolgen und deine Fitnessziele erreichen. Der Herzfrequenzmesser ermöglicht es dir, deine Herzfrequenz während des Trainings zu überwachen und deine Gesundheit im Auge zu behalten. Zusätzlich erhältst du Benachrichtigungen für Anrufe, Nachrichten und andere Apps direkt auf deinem Handgelenk. Die Smartwatch X1 ist ein unverzichtbarer Begleiter für ein aktives und vernetztes Leben.",
        "price": 199.99,
        "category": "Elektronik"
      },
      {
        "name": "Laptop ProBook 2000",
        "description": "Der Laptop ProBook 2000 ist ein leistungsstarker Laptop, der sowohl für den beruflichen als auch für den privaten Gebrauch geeignet ist. Mit einem Intel Core i5 Prozessor und 8 GB RAM bietet er eine schnelle und reibungslose Leistung. Die 500 GB SSD-Festplatte bietet ausreichend Speicherplatz für deine Dateien, Fotos und Videos. Das hochauflösende Display sorgt für eine klare und detaillierte Darstellung. Der Laptop ProBook 2000 ist ein zuverlässiges Arbeitsgerät für unterwegs oder zu Hause.",
        "price": 899.99,
        "category": "Elektronik"
      },
      {
        "name": "Kamera DSLR 3000",
        "description": "Die Kamera DSLR 3000 ist eine professionelle digitale Spiegelreflexkamera, die beeindruckende Fotos und Videos aufnimmt. Mit einem hochauflösenden Sensor und einem vielseitigen Objektivsystem ermöglicht sie dir, jeden Moment in hoher Qualität festzuhalten. Die Kamera DSLR 3000 verfügt über verschiedene manuelle Einstellungen, um deine kreativen Möglichkeiten zu erweitern. Mit ihrem robusten Gehäuse und ihrer benutzerfreundlichen Bedienung ist sie ideal für Hobbyfotografen und Profis gleichermaßen geeignet.",
        "price": 1499.99,
        "category": "Elektronik"
      },
      {
        "name": "Laufschuhe AirRun 500",
        "description": "Die Laufschuhe AirRun 500 sind speziell für Läufer entwickelt und bieten optimalen Komfort und Dämpfung. Mit einer atmungsaktiven Oberfläche und einer stoßdämpfenden Sohle sind sie ideal für lange Läufe und intensive Trainingseinheiten. Die Laufschuhe AirRun 500 verfügen über eine flexible Passform und unterstützen eine natürliche Abrollbewegung des Fußes. Egal, ob du ein erfahrener Läufer oder ein Anfänger bist, diese Schuhe werden dir helfen, deine Laufziele zu erreichen.",
        "price": 99.99,
        "category": "Sportartikel"
      },
      {
        "name": "Buchregal ClassicWood",
        "description": "Das Buchregal ClassicWood ist ein elegantes Möbelstück für dein Zuhause. Hergestellt aus hochwertigem Holz, bietet es ausreichend Platz für Bücher, Dekorationsgegenstände und mehr. Mit seinem zeitlosen Design fügt sich das Buchregal ClassicWood perfekt in jeden Raum ein und verleiht ihm einen Hauch von Eleganz. Es ist nicht nur funktional, sondern auch ein Blickfang, der deine persönliche Bibliothek stilvoll präsentiert.",
        "price": 249.99,
        "category": "Möbel"
      },
      {
        "name": "Handtasche Fashionista",
        "description": "Die Handtasche Fashionista ist eine modische und vielseitige Tasche, die sowohl praktisch als auch stilvoll ist. Mit mehreren Fächern und einer ausreichenden Größe bietet sie genügend Platz für deine persönlichen Gegenstände, wie Geldbörse, Handy und Make-up. Die Handtasche Fashionista besteht aus hochwertigem Kunstleder und verfügt über ein attraktives Design mit trendigen Details. Egal, ob du sie zur Arbeit, beim Einkaufen oder für besondere Anlässe trägst, diese Handtasche wird deinen Look perfekt ergänzen.",
        "price": 79.99,
        "category": "Mode"
      },
      {
        "name": "Fahrrad MountainRider",
        "description": "Das Fahrrad MountainRider ist ein robustes und leistungsfähiges Mountainbike, das für Abenteuer in jedem Gelände entwickelt wurde. Mit einer stabilen Rahmenkonstruktion und einer hochwertigen Federung bietet es eine geschmeidige Fahrt und optimale Kontrolle auf unebenen Wegen. Die Bremsen bieten zuverlässige Sicherheit, während die Schaltung schnelle und präzise Gangwechsel ermöglicht. Egal, ob du durch den Wald fährst oder auf Bergpfaden unterwegs bist, das Fahrrad MountainRider wird dich bei deinen Offroad-Abenteuern begleiten.",
        "price": 699.99,
        "category": "Sportartikel"
      },
      {
        "name": "Küchenmaschine CookMaster",
        "description": "Die Küchenmaschine CookMaster ist ein vielseitiges Küchengerät, das dir bei der Zubereitung von Mahlzeiten eine große Hilfe sein wird. Mit verschiedenen Aufsätzen und Funktionen, wie zum Beispiel einem Rührbesen, einem Knethaken und einem Mixeraufsatz, kannst du Teige kneten, Zutaten mixen und vieles mehr. Die Küchenmaschine CookMaster erleichtert dir das Kochen und Backen und spart dir Zeit und Mühe in der Küche. Sie ist ein unverzichtbares Werkzeug für Hobbyköche und Profis.",
        "price": 299.99,
        "category": "Haushaltsgeräte"
      },
      {
        "name": "Bluetooth-Lautsprecher SoundWave",
        "description": "Der Bluetooth-Lautsprecher SoundWave bietet kabellosen Musikgenuss in hoher Qualität. Mit einer kraftvollen Klangwiedergabe und einer langen Akkulaufzeit ist er ideal für Partys, Outdoor-Aktivitäten oder einfach zum Entspannen zu Hause. Der Bluetooth-Lautsprecher SoundWave ist kompakt und tragbar, sodass du ihn problemlos überallhin mitnehmen kannst. Verbinde ihn einfach mit deinem Smartphone oder Tablet und genieße deine Lieblingssongs in beeindruckender Klangqualität.",
        "price": 79.99,
        "category": "Elektronik"
      }

    ];

    for (const product of productData) {
      const productWithImage: Product = {
        imageUrl: exampleImageUrl,
        ...product
      };
  
      try {
        await this.productsService.createProduct(productWithImage);
      } catch (error) {
        console.error(`Error creating product:`, error);
      }
    }
  } 

  async onSubmit(form: NgForm): Promise<void> {
    if (form.valid) {
      if (!this.newProduct.imageUrl) {
        this.newProduct.imageUrl = this.defaultImageUrl;
      }
      try {
        await this.productsService.createProduct(this.newProduct);
        form.resetForm();
        this.showForm = false;
        await this.getProducts();
      } catch (error) {
        console.error(`Error creating product:`, error);
      }
    }
  }
}