import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.model';
import { ProductsService } from '../product-list/products.service';
import { ShoppingCartService } from './shopping-cart.service';
import { Location } from '@angular/common';



@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {

  private defaultImageUrl = 'https://media.istockphoto.com/id/1147544807/de/vektor/miniaturbild-vektorgrafik.jpg?s=612x612&w=0&k=20&c=IIK_u_RTeRFyL6kB1EMzBufT4H7MYT3g04sz903fXAk=';
  products: Product[] = [];

  constructor(
    private shoppingCartService: ShoppingCartService,
    private productsService: ProductsService
  ) { }

  ngOnInit(): void {
    this.loadProducts();
  }

  async loadProducts(): Promise<void> {
    try {
      const productIds = await this.shoppingCartService.getProductIdsByCartId('ogmkYzx5YMzDDlz37ywp');
      this.products = [];
  
      console.log(productIds);
  
      for (const productId of productIds) {

        console.log(productId); //hier
        const product = await this.productsService.getById(productId);
        console.log(product)
        if (product) {
          this.products.push(product);
        }
      }
    } catch (error) {
      console.error('Error loading products:', error);
    }
  }

  getTotalPrice(): number {
    return this.products.reduce((total, product) => total + product.price, 0);
  }

  async removeProduct(productId: string) {
    await this.shoppingCartService.removeProductIdFromCart(productId);
    window.location.reload();
  }
}