import { Injectable, inject } from '@angular/core';
import { Firestore, QuerySnapshot, addDoc, collection, collectionData, deleteDoc, doc, getDoc, getDocs, setDoc } from '@angular/fire/firestore';
import { ShoppingCart } from '../models/shopping-cart.models';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  firestore: Firestore = inject(Firestore);
  cartId: string = 'ogmkYzx5YMzDDlz37ywp';

  constructor() { }


  async getProductIdsByCartId(cartId: string): Promise<string[]> {
    const docRef = doc(this.firestore, 'cart', cartId);
    const docSnap = await getDoc(docRef);
  
    if (docSnap.exists()) {
      const cartData = docSnap.data() as ShoppingCart;
      const productIds: string[] = cartData.productIds;
      return productIds;
    } else {
      console.log("Cart does not exist");
      return [];
    }
  }
  async addProductIdsToCart(cartId: string, productIds: string[]): Promise<void> {
    try {
      const docRef = doc(this.firestore, 'cart', cartId);
      const docSnap = await getDoc(docRef);
  
      if (docSnap.exists()) {
        const cartData = docSnap.data() as ShoppingCart;
        const updatedProductIds = [...cartData.productIds, ...productIds];
  
        await setDoc(docRef, { productIds: updatedProductIds });
        console.log("Product IDs added to cart successfully");
      } else {
        console.log("Cart does not exist");
      }
    } catch (error) {
      console.error("Error adding product IDs to cart: ", error);
    }
  }

  async removeProductIdFromCart(productId: string): Promise<void> {
    try {
      const docRef = doc(this.firestore, 'cart', "ogmkYzx5YMzDDlz37ywp");
      const docSnap = await getDoc(docRef);
  
      if (docSnap.exists()) {
        const cartData = docSnap.data() as ShoppingCart;
        const updatedProductIds = cartData.productIds.filter(id => id !== productId);
  
        await setDoc(docRef, { productIds: updatedProductIds });
        console.log("Product ID removed from cart successfully");
      } else {
        console.log("Cart does not exist");
      }
    } catch (error) {
      console.error("Error removing product ID from cart: ", error);
    }
  }
}
