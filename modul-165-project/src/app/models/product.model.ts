export class Product{
    id?: string;
    name: string;
    description: string;
    price: number;
    category: string;
    imageUrl: string;
}