import { Component, OnInit } from '@angular/core';
import { initializeApp } from "firebase/app";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'modul-165-project';

  ngOnInit(): void {
    const app = initializeApp(this.firebaseConfig);
  }
  firebaseConfig = {
    apiKey: "AIzaSyA-v8ieJ7N90oE9MmtVsKZ8OKzWRyP8nOs",
    authDomain: "mogo-proje.firebaseapp.com",
    projectId: "mogo-proje",
    storageBucket: "mogo-proje.appspot.com",
    messagingSenderId: "324041275086",
    appId: "1:324041275086:web:5755f519403a8d0f07429f"
  };
}
